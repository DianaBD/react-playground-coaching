import React, {createContext} from 'react';
import {initialState, StateType} from "./models";

interface ContextState {
    state: StateType,
    setMoviesState: (newMoviesState: any) => void
}

const StateContext = createContext(initialState);

export default StateContext