export default (state: any, action: { type: any; payload: any; }) => {
    switch(action.type) {
        case 'SET_MOVIE_STATE':
            console.log(state)
            console.log(action)
            return {
                ...state, movieListState: action.payload
            }
        default:
            return state;
    }
}