import {Movie} from "../API";

export interface MovieListStateType {
    page: number;
    results: Movie[];
    total_pages: number;
    total_results: number;
}

export interface StateType {
    movieListState: MovieListStateType;
    setMoviesState: any;
}

export const initialMovieListState: MovieListStateType = {
    page: 0,
    results: [] as Movie[],
    total_pages: 0,
    total_results: 0
};

export const initialState: StateType = {
    movieListState: initialMovieListState,
    setMoviesState: () => {}
};

