
export function actions(dispatch:  React.Dispatch<{type: any, payload: any}>) {

    return {
        setMoviesState: (newMoviesState: any) => {
            dispatch({
                type: 'SET_MOVIE_STATE',
                payload: newMoviesState
            })}
    }
}
