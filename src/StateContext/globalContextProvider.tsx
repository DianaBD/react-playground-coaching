import React, {FC, ReactNode} from 'react'
import StateContext from "./index";
import {useReducer} from "react";
import {initialState} from "./models";
import reducer from "./reducer";
import {actions} from "./actions";

interface Props {
    children?: ReactNode
}

const GlobalProvider: FC<Props> = ({ children }) => {
    const [state, dispatch] = useReducer(reducer, initialState);

    // Actions for changing state
    const movieActions = actions(dispatch)

    return(
        <StateContext.Provider value = {{...state, ...movieActions}}>
            {children}
        </StateContext.Provider>
    )
}

export default GlobalProvider