export const LOCALES = {
    ENGLISH: 'en-us',
    GERMAN: 'de-de',
    FRENCH: 'fr-ca'
}

export const LOCALES_ARRAY = [
    'en-us', 'de-de', 'fr-ca'
]