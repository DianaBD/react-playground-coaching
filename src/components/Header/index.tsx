import React, {FC} from 'react';
import { Link } from 'react-router-dom';

import RMDBLogo from '../../images/react-movie-logo.svg';

import { Wrapper, Content, LogoImg, StyledRow, StyledMessage } from './Header.styles';
import LanguageSelect from "../LanguageSelect";
import {FormattedMessage} from "react-intl";
import {LOCALES_ARRAY} from "../../i18n/locales";

interface Props {
    setLocale:  React.Dispatch<React.SetStateAction<string>>;
}

const Header: FC<Props> = ({setLocale}) => {

    return(
        <>
            <Wrapper>
                <Content>
                    <Link to='/'>
                        <LogoImg src={RMDBLogo} alt='rmdb-logo'/>
                    </Link>
                    <StyledRow>
                        <StyledMessage>
                            <FormattedMessage id={'hello'}/>
                        </StyledMessage>
                        <LanguageSelect options={LOCALES_ARRAY} setLocale={setLocale}></LanguageSelect>
                    </StyledRow>
                </Content>
            </Wrapper>
        </>
    )
};

export default Header;
