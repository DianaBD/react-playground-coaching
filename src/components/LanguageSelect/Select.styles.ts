import styled from "styled-components";
import React from "react";

interface StyledSelectWrapperProps {
    ref:  React.RefObject<HTMLDivElement> | HTMLDivElement | null;
}

const StyledSelectWrapper = styled.div<StyledSelectWrapperProps>(({ref}) =>`
    width: 115px;
`);

const StyledSelect = styled.div`
    min-height: 40px;
    display: flex;
    align-items: center;
    padding-left: 15px;
    border: 1px solid grey;
    color: white;
    
    &:hover {
        background: white;
        color: black;
        cursor: pointer;
    }
`;

const StyledSelectOptionsWrapper = styled.div`
    background: white;
    position: absolute;
    z-index: 1;
`;

interface StyledSelectOptionProps {
    selected: boolean;
}

const StyledSelectOption = styled.div<StyledSelectOptionProps>(({selected}) =>`
    min-width: 115px;
    min-height: 40px;
    padding-left: 15px;
    display: flex;
    align-items: center;
    border-top: 1px solid grey;
    
    &:hover {
        background-color: grey;
        color: white;
        cursor: pointer;
    }
    
    ${selected && 'font-weight: bold'}
`);

const StyledIcon = styled.div`
    margin: 10px;
`;

export {
    StyledSelectWrapper,
    StyledSelect,
    StyledSelectOptionsWrapper,
    StyledSelectOption,
    StyledIcon
}