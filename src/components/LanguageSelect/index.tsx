import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCaretDown} from "@fortawesome/free-solid-svg-icons/faCaretDown";
import React, {FC, useEffect, useRef, useState} from 'react';
import { StyledSelectWrapper, StyledSelect, StyledSelectOptionsWrapper, StyledSelectOption, StyledIcon } from "./Select.styles";
import {LOCALES} from "../../i18n";
import {LOCALES_ARRAY} from "../../i18n/locales";

interface Props {
    options: any[];
    setLocale: React.Dispatch<React.SetStateAction<string>>;
}

const LanguageSelect: FC<Props> = ({options,setLocale}) => {
    const [selected, setSelected] = useState(LOCALES.ENGLISH);
    const [showOptions, setShowOptions] = useState(false);

    const langSelectRef = useRef<HTMLDivElement>(null);

    const handleClickOutside = (event: MouseEvent) => {
        if(langSelectRef.current && !langSelectRef.current.contains(event.target as HTMLDivElement) && showOptions) {
            toggleOptions()
        }
    }

    const toggleOptions = () => {
        setShowOptions(!showOptions);
    }

    const onSelect = (value: string) => {
        setSelected(value);
        setLocale(value);
    }

    useEffect(() => {
        document.addEventListener('click', handleClickOutside, true);
        return () => document.removeEventListener('click', handleClickOutside, true);
    },[handleClickOutside]);

    return(
        <StyledSelectWrapper ref={langSelectRef}>
            <StyledSelect onClick={toggleOptions}>Ln: {selected}
                <StyledIcon>
                    <FontAwesomeIcon icon={faCaretDown} />
                </StyledIcon>
            </StyledSelect>
            {showOptions &&
                <StyledSelectOptionsWrapper>
                    {options && options.map((option,index) =>
                        <StyledSelectOption onClick={() => onSelect(option)} key={index} selected={option.localeCompare(selected) === 0}>{option}</StyledSelectOption>
                    )}
                </StyledSelectOptionsWrapper>
            }
        </StyledSelectWrapper>
    )

};

export default LanguageSelect;