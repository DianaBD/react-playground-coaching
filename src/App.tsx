import React, {useReducer, useState} from 'react';
// Routing
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
// Components
import Header from './components/Header';
import Home from './components/Home';
import Movie from './components/Movie';
import NotFound from './components/NotFound';

import {I18nProvider, LOCALES} from "./i18n";

// Styles
import { GlobalStyle } from './GlobalStyle';
import StateContext from "./StateContext";
import {initialState} from "./StateContext/models";
import reducer from "./StateContext/reducer";
import GlobalProvider from "./StateContext/globalContextProvider";

const App: React.FC = () => {
    const [currentLocale, setLocale] = useState(LOCALES.ENGLISH);

    return (
        <Router>
            <I18nProvider locale={currentLocale}>
                <GlobalProvider>
                    <Header setLocale={setLocale}/>
                    <Routes>
                        <Route path='/' element={<Home/>}/>
                        <Route path='/:movieId' element={<Movie/>}/>
                        <Route path='/*' element={<NotFound/>}/>
                    </Routes>
                </GlobalProvider>
                <GlobalStyle/>
            </I18nProvider>
        </Router>
    )
};

export default App;
